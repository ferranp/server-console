#!/bin/bash


DIALOG=dialog
TMP=`tempfile`


while true;
do

  BACKTITLE=`uname -a` 
  TITLE=`hostname` 

  $DIALOG --clear --title "$TITLE" \
          --timeout 2 \
          --backtitle "$BACKTITLE" \
          --menu "" 0 0 0  \
          reboot "Reiniciar servidor" \
          halt   "Parar servidor" \
	  2> $TMP

  retval=$?
  action=`cat $TMP`

  case $retval in  
	0) 
        case $action in
            reboot)
               reboot
               ;;
            halt)
               shutdown -h now
        esac 
        sleep 1 
        ;; 
	255) 
        echo "TIMEOUT OR ESC"
        ;; 
        *)
        echo $retval
        exit 2
  esac       
done
